package it.uniroma3.siw.project.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.project.model.Copia;

public interface CopiaRepository extends CrudRepository<Copia, Long> {

}
