package it.uniroma3.siw.project.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.project.model.Gioco;

public interface GiocoRepository extends CrudRepository<Gioco, Long> {

	public Gioco findByNome(String nome);

}
