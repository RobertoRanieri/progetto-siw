package it.uniroma3.siw.project.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.project.model.OggettoCollezionabile;;

public interface OggettoCollezionabileRepository extends CrudRepository<OggettoCollezionabile, Long> {

}
