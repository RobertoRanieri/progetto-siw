package it.uniroma3.siw.project.repository;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import it.uniroma3.siw.project.model.Accessorio;
import it.uniroma3.siw.project.model.Copia;
import it.uniroma3.siw.project.model.Console;
import it.uniroma3.siw.project.model.Gioco;
import it.uniroma3.siw.project.model.Immagine;
import it.uniroma3.siw.project.model.Piattaforma;
import it.uniroma3.siw.project.model.TipoImmagine;
import it.uniroma3.siw.project.model.Utente;
import it.uniroma3.siw.project.model.ValutazioneESRB;

/* è un componente dell'applicazione */
@Component
public class DBPopulation implements ApplicationRunner {

	@Autowired
	private UtenteRepository utenteRepository;
	@Autowired
	private GiocoRepository giocoRepository;
	@Autowired
	private PiattaformaRepository piattaformaRepository;
	@Autowired
	private AccessorioRepository accessorioRepository;
	@Autowired
	private ConsoleRepository consoleRepository;
	@Autowired
	private ImmagineRepository immagineRepository;
	@Autowired
	private PasswordEncoder passEncoder;
	@Autowired
	private CopiaRepository copiaRepository;

	private void deleteAll() {
		System.out.print("Cancello tutto");
		utenteRepository.deleteAll();
		giocoRepository.deleteAll();
		piattaformaRepository.deleteAll();
		accessorioRepository.deleteAll();
		consoleRepository.deleteAll();
		immagineRepository.deleteAll();
	}

	private void addAll() {

		// Aggiunta delle immagini
		Immagine img1 = new Immagine("https://cdn.thegamesdb.net/images/original/boxart/front/908-2.jpg",
				TipoImmagine.FRONTCOVER);
		Immagine img2 = new Immagine("https://cdn.thegamesdb.net/images/original/boxart/back/908-2.jpg",
				TipoImmagine.BACKCOVER);
		Immagine img3 = new Immagine("https://cdn.thegamesdb.net/images/original/fanart/908-4.jpg",
				TipoImmagine.FANART);
		Immagine img4 = new Immagine("https://cdn.thegamesdb.net/images/original/screenshots/908-1.jpg",
				TipoImmagine.SCREENSHOT);
		Immagine img5 = new Immagine("https://cdn.thegamesdb.net/images/original/clearlogo/908.png", TipoImmagine.LOGO);
		Immagine img6 = new Immagine("https://cdn.thegamesdb.net/images/original/graphical/908-g.jpg",
				TipoImmagine.BANNER);
		List<Immagine> immaginiMarioGalaxy = new ArrayList<Immagine>();
		immaginiMarioGalaxy.add(img1);
		immaginiMarioGalaxy.add(img2);
		immaginiMarioGalaxy.add(img3);
		immaginiMarioGalaxy.add(img4);
		immaginiMarioGalaxy.add(img5);
		immaginiMarioGalaxy.add(img6);
		immagineRepository.saveAll(immaginiMarioGalaxy);
		Immagine img7 = new Immagine("https://upload.wikimedia.org/wikipedia/commons/1/14/Wii-console.jpg",
				TipoImmagine.PHOTO);
		immagineRepository.save(img7);
		Immagine img8 = new Immagine(
				"https://upload.wikimedia.org/wikipedia/commons/5/53/Wii_nunchuk_controller_side.jpg",
				TipoImmagine.PHOTO);
		immagineRepository.save(img8);
		Immagine img9 = new Immagine("https://cdn.thegamesdb.net/images/original/boxart/front/35049-1.jpg",TipoImmagine.FRONTCOVER);
		Immagine img10 = new Immagine("https://cdn.thegamesdb.net/images/original/boxart/back/35049-1.jpg",TipoImmagine.BACKCOVER);
		Immagine img11 = new Immagine("https://cdn.thegamesdb.net/images/original/screenshots/35049-1.jpg",TipoImmagine.SCREENSHOT);
		Immagine img12 = new Immagine("https://cdn.thegamesdb.net/images/original/screenshots/35049-6.jpg",TipoImmagine.BANNER);
		Immagine img13 = new Immagine("https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Persona_5_logo.svg/500px-Persona_5_logo.svg.png",TipoImmagine.LOGO);
		Immagine img14 = new Immagine("https://cdn.thegamesdb.net/images/original/screenshots/35049-2.jpg", TipoImmagine.FANART);
		List<Immagine> immaginiPersona5 = new ArrayList<Immagine>();
		immaginiPersona5.add(img9);
		immaginiPersona5.add(img10);
		immaginiPersona5.add(img11);
		immaginiPersona5.add(img12);
		immaginiPersona5.add(img13);
		immaginiPersona5.add(img14);
		immagineRepository.saveAll(immaginiPersona5);
		Immagine img15 = new Immagine("https://upload.wikimedia.org/wikipedia/commons/a/a5/Sony-PlayStation4-Pro-Console-FL.jpg",TipoImmagine.PHOTO);
		immagineRepository.save(img15);
		Immagine img16 = new Immagine("https://upload.wikimedia.org/wikipedia/commons/c/c8/Dualshock_4.jpg", TipoImmagine.PHOTO);
		immagineRepository.save(img16);

		//Aggiunta delle piattaforme
		Piattaforma piatt1 = new Piattaforma("Nintendo Wii", "Il Nintendo Wii è una console per videogiochi, prodotta dall'azienda giapponese Nintendo, lanciata sul mercato nel 2006, come successore del Nintendo GameCube.\r\n" + 
				"Si tratta della console casalinga più venduta tra quelle della settima generazione e di tutte quelle precedenti di Nintendo, nonché di una delle console più diffuse al mondo. Il Wii ha rappresentato il principale concorrente della Xbox 360 di Microsoft e della PlayStation 3 di Sony. ");
		this.piattaformaRepository.save(piatt1);
		Piattaforma piatt2 = new Piattaforma("Playstation 4", "La PlayStation 4 (abbreviata con la sigla PS4) è una console per videogiochi creata dalla Sony Computer Entertainment, dotata di varie funzioni multimediali oltre a quelle di intrattenimento videoludico. Annunciata come successore della PlayStation 3 da Andrew House il 20 febbraio 2013 in Europa durante il PlayStation Meeting, tenutosi a Manhattan, New York, la console è disponibile dal 15 novembre del 2013 nel Nord America e dal 29 novembre del 2013 in Europa e Sud America, mentre in Giappone dal 22 febbraio del 2014 includendo la versione digitale del gioco Knack. Fa parte dell'ottava generazione di console, e compete commercialmente con Wii U di Nintendo e Xbox One di Microsoft. ");
		this.piattaformaRepository.save(piatt2);
		Piattaforma piatt3 = new Piattaforma("Steam", "Steam è una piattaforma sviluppata da Valve che si occupa di distribuzione digitale, di gestione dei diritti digitali, di modalità di gioco multi giocatore e di comunicazione. Viene usata per gestire e distribuire una vasta gamma di giochi (alcuni esclusivi) e il loro relativo supporto. Tutte queste operazioni sono effettuate via Internet. ");
		this.piattaformaRepository.save(piatt3);

		// Aggiunta dei giochi
		LocalDate dataRilascio = LocalDate.of(2010, Month.MAY, 27);
		String genere1 = "Platform";
		Gioco gioco1 = new Gioco("Super Mario Galaxy 2", dataRilascio, piatt1,
				"Super Mario Galaxy 2 è un videogioco per la Wii sviluppato da Nintendo. È il tredicesimo videogioco della serie di Mario ed è stato pubblicato il 23 maggio 2010 in America, il 27 maggio in Giappone, l'11 giugno in Europa e il 1º luglio in Australia. Fu annunciato all'E3 del 2009. Si tratta del quarto gioco in grafica tridimensionale della serie di Mario dopo Super Mario 64, Super Mario Sunshine e Super Mario Galaxy.",
				ValutazioneESRB.EVERYONE, genere1, 2, false, "O7steQttNTo", "Nintendo", "Nintendo", immaginiMarioGalaxy);
		this.giocoRepository.save(gioco1);
		LocalDate dataRilascio2 = LocalDate.of(2016, Month.SEPTEMBER, 15);
		String genere2="J-Rpg";
		Gioco gioco2 = new Gioco("Persona 5", dataRilascio2, piatt2, "Persona 5 è un videogioco di ruolo e simulatore di vita per PlayStation 3 e PlayStation 4. È il sesto titolo della serie videoludica Persona, creata dalla Atlus.", ValutazioneESRB.MATURE, 
				genere2, 1, false, "QnDzJ9KzuV4", "Atlus", "Atlus", immaginiPersona5);
		this.giocoRepository.save(gioco2);

		// Aggiunta delle console
		Console c1 = new Console("Nintendo Wii Bianca", piatt1, img7);
		this.consoleRepository.save(c1);
		Console c2 = new Console("Playstation 4 Pro", piatt2, img15);
		this.consoleRepository.save(c2);

		// Aggiunta degli accessori
		Accessorio a1 = new Accessorio("Nunchuck Bianco", piatt1, img8);
		this.accessorioRepository.save(a1);
		Accessorio a2 = new Accessorio("Dualshock 4", piatt2, img16);
		this.accessorioRepository.save(a2);

		// Aggiunta degli utenti
		Utente us1 = new Utente("robertom.ranieri@gmail.com", this.passEncoder.encode("PasswordProva!4"), "ADMIN");
		Utente us2 = new Utente("ann.lanzilli@stud.uniroma3.it", this.passEncoder.encode("PasswordDiProva!1"), "USER");
		// TODO
		Copia copia1 = new Copia(gioco1);
		copia1.setOggettoRiferimento(gioco1);
		this.copiaRepository.save(copia1);
		us2.addCopia(copia1);
		utenteRepository.save(us1);
		utenteRepository.save(us2);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		deleteAll();
		addAll();
	}

}