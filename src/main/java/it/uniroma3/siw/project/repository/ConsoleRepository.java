package it.uniroma3.siw.project.repository;

import org.springframework.data.repository.CrudRepository;
import it.uniroma3.siw.project.model.Console;

public interface ConsoleRepository extends CrudRepository<Console, Long> {

	public Console findByNome(String nome);

}
