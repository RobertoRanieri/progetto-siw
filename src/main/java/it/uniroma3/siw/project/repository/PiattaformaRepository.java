package it.uniroma3.siw.project.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.project.model.Piattaforma;

public interface PiattaformaRepository extends CrudRepository<Piattaforma, Long> {

	public Piattaforma findByNome(String nome);

}
