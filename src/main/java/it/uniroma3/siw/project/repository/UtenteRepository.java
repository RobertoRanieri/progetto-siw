package it.uniroma3.siw.project.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.project.model.Utente;

public interface UtenteRepository extends CrudRepository<Utente, Long> {

	public Utente findByEmailAndPassword(String email, String psw);

	public Utente findByEmail(String email);

}
