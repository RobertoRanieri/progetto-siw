package it.uniroma3.siw.project.repository;

import org.springframework.data.repository.CrudRepository;
import it.uniroma3.siw.project.model.Accessorio;

public interface AccessorioRepository extends CrudRepository<Accessorio, Long> {

	public Accessorio findByNome(String nome);

}
