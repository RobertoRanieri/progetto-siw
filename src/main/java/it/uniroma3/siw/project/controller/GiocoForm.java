package it.uniroma3.siw.project.controller;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import it.uniroma3.siw.project.model.Piattaforma;
import it.uniroma3.siw.project.model.ValutazioneESRB;

public class GiocoForm {
	private String nome;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataRilascio;
	private Piattaforma piattaforma;
	private String descrizione;
	private ValutazioneESRB valutazioneEta;
	private String genere;
	private int numeroMassimoGiocatori;
	private boolean haModalitaCoop;
	private String linkVideoYoutube;
	private String editore;
	private String sviluppatore;
	private String urlFRONTCOVER;
	private String urlBACKCOVER;
	private String urlSCREENSHOT;
	private String urlLOGO;
	private String urlFANART;
	private String urlBANNER;
	// tutti questi campi immagini possono pure diventare liste
	
	public GiocoForm() {
		this.haModalitaCoop=false;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDate getDataRilascio() {
		return dataRilascio;
	}

	public void setDataRilascio(LocalDate dataRilascio) {
		this.dataRilascio = dataRilascio;
	}

	public Piattaforma getPiattaforma() {
		return piattaforma;
	}

	public void setPiattaforma(Piattaforma piattaforma) {
		this.piattaforma = piattaforma;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public ValutazioneESRB getValutazioneEta() {
		return valutazioneEta;
	}

	public void setValutazioneEta(ValutazioneESRB valutazioneEta) {
		this.valutazioneEta = valutazioneEta;
	}

	public String getGenere() {
		return genere;
	}

	public void setGenere(String genere) {
		this.genere = genere;
	}

	public int getNumeroMassimoGiocatori() {
		return numeroMassimoGiocatori;
	}

	public void setNumeroMassimoGiocatori(int numeroMassimoGiocatori) {
		this.numeroMassimoGiocatori = numeroMassimoGiocatori;
	}

	public boolean isHaModalitaCoop() {
		return haModalitaCoop;
	}

	public void setHaModalitaCoop(boolean haModalitaCoop) {
		this.haModalitaCoop = haModalitaCoop;
	}

	public String getLinkVideoYoutube() {
		return linkVideoYoutube;
	}

	public void setLinkVideoYoutube(String linkVideoYoutube) {
		this.linkVideoYoutube = linkVideoYoutube;
	}

	public String getEditore() {
		return editore;
	}

	public void setEditore(String editore) {
		this.editore = editore;
	}

	public String getSviluppatore() {
		return sviluppatore;
	}

	public void setSviluppatore(String sviluppatore) {
		this.sviluppatore = sviluppatore;
	}

	public String getUrlFRONTCOVER() {
		return urlFRONTCOVER;
	}

	public void setUrlFRONTCOVER(String urlFRONTCOVER) {
		this.urlFRONTCOVER = urlFRONTCOVER;
	}

	public String getUrlBACKCOVER() {
		return urlBACKCOVER;
	}

	public void setUrlBACKCOVER(String urlBACKCOVER) {
		this.urlBACKCOVER = urlBACKCOVER;
	}

	public String getUrlSCREENSHOT() {
		return urlSCREENSHOT;
	}

	public void setUrlSCREENSHOT(String urlSCREENSHOT) {
		this.urlSCREENSHOT = urlSCREENSHOT;
	}

	public String getUrlLOGO() {
		return urlLOGO;
	}

	public void setUrlLOGO(String urlLOGO) {
		this.urlLOGO = urlLOGO;
	}

	public String getUrlFANART() {
		return urlFANART;
	}

	public void setUrlFANART(String urlFANART) {
		this.urlFANART = urlFANART;
	}

	public String getUrlBANNER() {
		return urlBANNER;
	}

	public void setUrlBANNER(String urlBANNER) {
		this.urlBANNER = urlBANNER;
	}
}
