package it.uniroma3.siw.project.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.project.model.Gioco;
import it.uniroma3.siw.project.model.Immagine;
import it.uniroma3.siw.project.model.TipoImmagine;
import it.uniroma3.siw.project.model.ValutazioneESRB;
import it.uniroma3.siw.project.services.GiocoService;
import it.uniroma3.siw.project.services.GiocoValidator;
import it.uniroma3.siw.project.services.ImmagineService;
import it.uniroma3.siw.project.services.PiattaformaService;

@Controller
public class GiocoController {
	@Autowired
	private GiocoValidator giocoValidator;
	@Autowired
	private GiocoService giocoService;
	@Autowired
	private PiattaformaService piattaformaService;
	@Autowired
	private ImmagineService immagineService;

	// ------------------------------ OPERAZIONI ADMIN
	// ---------------------------------

	@RequestMapping("/admin/iniziaAggiuntaGioco")
	public String iniziaAggiuntaGioco(Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		model.addAttribute("form", new GiocoForm());
		model.addAttribute("piattaforme", this.piattaformaService.tutti());
		model.addAttribute("rating", ValutazioneESRB.values());
		return "aggiungiGioco.html";
	}

	@RequestMapping(value = "/admin/registraNuovoGioco", method = RequestMethod.POST)
	public String registraNuovoGioco(@Valid @ModelAttribute("form") GiocoForm form, Model model,
			BindingResult bindingResult) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		Gioco gioco = new Gioco(form.getNome(), form.getDataRilascio(), form.getPiattaforma(), form.getDescrizione(),
				form.getValutazioneEta(), form.getGenere(), form.getNumeroMassimoGiocatori(), form.isHaModalitaCoop(),
				form.getLinkVideoYoutube(), form.getEditore(), form.getSviluppatore(), new ArrayList<Immagine>());
		this.giocoValidator.validate(gioco, bindingResult);
		if (!bindingResult.hasErrors()) {
			if (!form.getUrlFRONTCOVER().equals("")) {
				Immagine frontCover = new Immagine(form.getUrlFRONTCOVER(), TipoImmagine.FRONTCOVER);
				gioco.addImmagini(frontCover);
				this.immagineService.inserisci(frontCover);
			}
			if (!form.getUrlBACKCOVER().equals("")) {
				Immagine backCover = new Immagine(form.getUrlBACKCOVER(), TipoImmagine.BACKCOVER);
				gioco.addImmagini(backCover);
				this.immagineService.inserisci(backCover);
			}
			if (!form.getUrlSCREENSHOT().equals("")) {
				Immagine screenshot = new Immagine(form.getUrlSCREENSHOT(), TipoImmagine.SCREENSHOT);
				gioco.addImmagini(screenshot);
				this.immagineService.inserisci(screenshot);
			}
			if (!form.getUrlFANART().equals("")) {
				Immagine fanart = new Immagine(form.getUrlFANART(), TipoImmagine.FANART);
				gioco.addImmagini(fanart);
				this.immagineService.inserisci(fanart);
			}
			if (!form.getUrlLOGO().equals("")) {
				Immagine logo = new Immagine(form.getUrlLOGO(), TipoImmagine.LOGO);
				gioco.addImmagini(logo);
				this.immagineService.inserisci(logo);
			}
			if (!form.getUrlBANNER().equals("")) {
				Immagine banner = new Immagine(form.getUrlBANNER(), TipoImmagine.BANNER);
				gioco.addImmagini(banner);
				this.immagineService.inserisci(banner);
			}
			model.addAttribute(this.giocoService.inserisci(gioco));
			return "gioco.html";
		} else {
			model.addAttribute("form", new GiocoForm());
			model.addAttribute("piattaforme", this.piattaformaService.tutti());
			model.addAttribute("rating", ValutazioneESRB.values());
			return "aggiungiGioco.html";
		}
	}

	@RequestMapping(value = "/gioco/{id}", method = RequestMethod.GET)
	public String getGioco(@PathVariable("id") Long id, Model model) {
		if(!SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString().equals("anonymousUser")) {
			UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		}
		if (id != null) {
			Gioco g1 = this.giocoService.giocoPerId(id);
			if (g1.getImmagini() != null) {
				for (Immagine i : g1.getImmagini()) {
					switch (i.getTipoImmagine()) {
					case BACKCOVER:
						model.addAttribute("backCover", i);
						break;
					case BANNER:
						model.addAttribute("banner", i);
						break;
					case FANART:
						model.addAttribute("fanArt", i);
						break;
					case FRONTCOVER:
						model.addAttribute("frontCover", i);
						break;
					case LOGO:
						model.addAttribute("logo", i);
						break;
					case SCREENSHOT:
						model.addAttribute("screenshot", i);
						break;
					default:
						break;
					}
				}
			}
			if (g1.getValutazioneEta() != null) {
				switch (g1.getValutazioneEta()) {
				case ADULTSONLY:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/ESRB_Adults_Only_18%2B.svg/200px-ESRB_Adults_Only_18%2B.svg.png");
					break;
				case EVERYONE:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/ESRB_Everyone.svg/200px-ESRB_Everyone.svg.png");
					break;
				case EVERYONE10PLUS:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/ESRB_Everyone_10%2B.svg/200px-ESRB_Everyone_10%2B.svg.png");
					break;
				case MATURE:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/ESRB_Mature_17%2B.svg/200px-ESRB_Mature_17%2B.svg.png");
					break;
				case RATINGPENDING:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/ESRB_RP.svg/200px-ESRB_RP.svg.png");
					break;
				case TEEN:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/ESRB_Teen.svg/200px-ESRB_Teen.svg.png");
					break;
				case EARLYCHILDHOOD:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/ESRB_Early_Childhood.svg/200px-ESRB_Early_Childhood.svg.png");
					break;
				default:
					break;
				}
			}
			model.addAttribute("gioco", g1);
			return "gioco.html";
		} else {
			return "giochiUtente.html";
		}
	}

	// ------------------------ VISUALIZZAZIONE TUTTI ------------------------------
	@RequestMapping(value = "/giochi", method = RequestMethod.GET)
	public String getGiochi(Model model) {
		if(!SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString().equals("anonymousUser")) {
			UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		}
		model.addAttribute("giochi", this.giocoService.tutti());
		return "giochi.html";
	}

}
