package it.uniroma3.siw.project.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import it.uniroma3.siw.project.model.Piattaforma;
import it.uniroma3.siw.project.services.PiattaformaService;
import it.uniroma3.siw.project.services.PiattaformaValidator;

@Controller
public class PiattaformaController {

	@Autowired
	private PiattaformaValidator piattaformaValidator;

	@Autowired
	private PiattaformaService piattaformaService;

	// ------------------------- OPERAZIONI ADMIN -------------------

	@RequestMapping("/admin/iniziaAggiuntaPiattaforma")
	public String iniziaAggiuntaPiattaforma(Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		model.addAttribute("piattaforma", new Piattaforma());
		return "aggiungiPiattaforma.html";
	}

	@RequestMapping(value = "/admin/registraNuovaPiattaforma", method = RequestMethod.POST)
	public String registraNuovaPiattaforma(@Valid @ModelAttribute("piattaforma") Piattaforma piattaforma, Model model,
			BindingResult bindingResult) {
		this.piattaformaValidator.validate(piattaforma, bindingResult);
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		if (!bindingResult.hasErrors()) {
			this.piattaformaService.inserisci(piattaforma);
			return "piattaforma.html";
		} else {
			return "aggiungiPiattaforma.html";
		}
	}

	// ------------------------ VISUALIZZAZIONE SINGOLO
	// ------------------------------
	@RequestMapping(value = "/piattaforma/{id}", method = RequestMethod.GET)
	public String getPiattaforma(@PathVariable("id") Long id, Model model) {
		if(!SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString().equals("anonymousUser")) {
			UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		}
		if (id != null) {
			model.addAttribute("piattaforma", this.piattaformaService.trovaPiattaforma(id));
			return "piattaforma.html";
		} else {
			// TODO: When this has an actual use case, change it
			return "index.html";
		}
	}

	// ------------------------ VISUALIZZAZIONE TUTTI ------------------------------
	@RequestMapping(value = "/piattaforme", method = RequestMethod.GET)
	public String getPiattaforme(Model model) {
		if(!SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString().equals("anonymousUser")) {
			UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		}
		model.addAttribute("piattaforme", this.piattaformaService.tutti());
		return "piattaforme.html";
	}

}
