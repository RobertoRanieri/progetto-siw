package it.uniroma3.siw.project.controller;

import java.util.ArrayList;
import java.util.List;

public class CheckBoxForm {
	private List<Long> ids;

	// keeping this lets use one class to get the list of objects ids returned by
	// checkboxes

	public CheckBoxForm() {
		this.ids = new ArrayList<>();
	}

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

}
