package it.uniroma3.siw.project.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.project.model.Console;
import it.uniroma3.siw.project.model.TipoImmagine;
import it.uniroma3.siw.project.services.ConsoleService;
import it.uniroma3.siw.project.services.ConsoleValidator;
import it.uniroma3.siw.project.services.ImmagineService;
import it.uniroma3.siw.project.services.PiattaformaService;

@Controller
public class ConsoleController {

	@Autowired
	private ConsoleService consoleService;
	@Autowired
	private ConsoleValidator consoleValidator;
	@Autowired
	private PiattaformaService piattaformaService;
	@Autowired
	private ImmagineService immagineService;

	// ----------------------- OPERAZIONI ADMIN ----------------------------

	@RequestMapping("/admin/iniziaAggiuntaConsole")
	public String iniziaAggiuntaconsole(Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		model.addAttribute("console", new Console());
		model.addAttribute("piattaforme", this.piattaformaService.tutti());
		return "aggiungiConsole.html";
	}

	@RequestMapping(value = "/admin/registraNuovaConsole", method = RequestMethod.POST)
	public String registraNuovoconsole(@Valid @ModelAttribute("console") Console console, Model model,
			BindingResult bindingResult) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		this.consoleValidator.validate(console, bindingResult);
		if (!bindingResult.hasErrors()) {
			console.getImmagine().setTipoImmagine(TipoImmagine.PHOTO);
			this.immagineService.inserisci(console.getImmagine());
			model.addAttribute(this.consoleService.inserisci(console));
			return "console.html";
		} else {
			return "aggiungiConsole.html";
		}
	}

	// ------------------------ VISUALIZZAZIONE SINGOLO
	// ------------------------------
	@RequestMapping(value = "/console/{id}", method = RequestMethod.GET)
	public String getconsole(@PathVariable("id") Long id, Model model) {
		if(!SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString().equals("anonymousUser")) {
			UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		}
		if (id != null) {
			model.addAttribute("console", this.consoleService.consolePerId(id));
			return "console.html";
		} else {
			return "consoleUtente.html";
		}
	}

	// ------------------------ VISUALIZZAZIONE TUTTI ------------------------------
	@RequestMapping(value = "/consoles", method = RequestMethod.GET)
	public String getConsoles(Model model) {
		if(!SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString().equals("anonymousUser")) {
			UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		}
		model.addAttribute("consoles", this.consoleService.tutti());
		return "consoles.html";
	}

}
