package it.uniroma3.siw.project.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.project.model.Accessorio;
import it.uniroma3.siw.project.model.TipoImmagine;
import it.uniroma3.siw.project.services.AccessorioService;
import it.uniroma3.siw.project.services.AccessorioValidator;
import it.uniroma3.siw.project.services.ImmagineService;
import it.uniroma3.siw.project.services.PiattaformaService;

@Controller
public class AccessorioController {

	@Autowired
	private AccessorioService accessorioService;
	@Autowired
	private AccessorioValidator accessorioValidator;
	@Autowired
	private PiattaformaService piattaformaService;
	@Autowired
	private ImmagineService immagineService;

	// ---------------------- OPERAZIONI ADMIN --------------------------

	@RequestMapping("/admin/iniziaAggiuntaAccessorio")
	public String iniziaAggiuntaaccessorio(Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		model.addAttribute("accessorio", new Accessorio());
		model.addAttribute("piattaforme", this.piattaformaService.tutti());
		return "aggiungiAccessorio.html";
	}

	@RequestMapping(value = "/admin/registraNuovoAccessorio", method = RequestMethod.POST)
	public String registraNuovoaccessorio(@Valid @ModelAttribute("accessorio") Accessorio accessorio, Model model,
			BindingResult bindingResult) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		this.accessorioValidator.validate(accessorio, bindingResult);
		if (!bindingResult.hasErrors()) {
			accessorio.getImmagine().setTipoImmagine(TipoImmagine.PHOTO);
			this.immagineService.inserisci(accessorio.getImmagine());
			model.addAttribute(this.accessorioService.inserisci(accessorio));
			return "accessorio.html";
		} else {
			return "aggiungiAccessorio.html";
		}
	}

	// ------------------------ VISUALIZZAZIONE SINGOLO
	// ------------------------------
	@RequestMapping(value = "/accessorio/{id}", method = RequestMethod.GET)
	public String getaccessorio(@PathVariable("id") Long id, Model model) {
		if(!SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString().equals("anonymousUser")) {
			UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		}
		if (id != null) {
			model.addAttribute("accessorio", this.accessorioService.accessorioPerId(id));
			return "accessorio.html";
		} else {
			return "accessorioUtente.html";
		}
	}

	// ------------------------ VISUALIZZAZIONE TUTTI ------------------------------
	@RequestMapping(value = "/accessori", method = RequestMethod.GET)
	public String getAccessori(Model model) {
		if(!SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString().equals("anonymousUser")) {
			UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		}
		model.addAttribute("accessori", this.accessorioService.tutti());
		return "accessori.html";
	}
}
