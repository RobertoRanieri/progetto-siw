package it.uniroma3.siw.project.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import it.uniroma3.siw.project.model.Accessorio;
import it.uniroma3.siw.project.model.Copia;
import it.uniroma3.siw.project.model.Console;
import it.uniroma3.siw.project.model.Gioco;
import it.uniroma3.siw.project.model.Immagine;
import it.uniroma3.siw.project.model.Utente;
import it.uniroma3.siw.project.services.AccessorioService;
import it.uniroma3.siw.project.services.ConsoleService;
import it.uniroma3.siw.project.services.CopiaService;
import it.uniroma3.siw.project.services.GiocoService;
import it.uniroma3.siw.project.services.UtenteService;
import it.uniroma3.siw.project.services.UtenteValidator;

@Controller
@SessionAttributes("copia")
public class UtenteController {
	@Autowired
	private UtenteService utenteService;
	@Autowired
	private UtenteValidator utenteValidator;
	@Autowired
	private GiocoService giocoService;
	@Autowired
	private ConsoleService consoleService;
	@Autowired
	private AccessorioService accessorioService;
	@Autowired
	private CopiaService copiaService;

	@RequestMapping("/")
	public String slash(Model model) {
		if(!SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString().equals("anonymousUser")) {
			UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		}
		return "index.html";
	}
	
	@RequestMapping("/index")
	public String index(Model model) {
		if(!SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString().equals("anonymousUser")) {
			UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		}
		return "index.html";
	}

	// ------------------------------- REGISTRAZIONE
	// ---------------------------------

	@RequestMapping(value = "/iniziaRegistrazione", method = RequestMethod.GET)
	public String iniziaRegistrazioneNuovoUtente(Model model) {
		model.addAttribute("utente", new Utente());
		return "nuovoUtente.html";
	}

	@RequestMapping(value = "/registraNuovoUtente", method = RequestMethod.POST)
	public String registraNuovoUtente(@Valid @ModelAttribute("utente") Utente utente, Model model,
			BindingResult bindingResult) {
		this.utenteValidator.validate(utente, bindingResult);
		this.utenteValidator.registrabile(utente, bindingResult);
		if (!bindingResult.hasErrors()) {
			utente.setRole("USER");
			this.utenteService.salva(utente);
			return "successoRegistrazione.html"; // TODO potrebbe essere il caso di mettere una conferma dei dati?
		} else {
			return "nuovoUtente.html";
		}
	}

	// -------------------------------- AUTENTICAZIONE
	// --------------------------------

	@RequestMapping(value = "/utente/dashboard", method = RequestMethod.GET)
	public String dashUtente(Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("email", utenteDet.getUsername());
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		return "dashboardUtente.html";
	}
	
	@RequestMapping(value = "/admin/dashboard", method = RequestMethod.GET)
	public String dashAdmin(Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("email", utenteDet.getUsername());
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		return "dashboardAdmin.html";
	}

	// ------------------------------ VISUALIZZARE LISTE
	// ------------------------------

	@RequestMapping(value = "/utente/listaGiochi", method = RequestMethod.GET)
	public String listaGiochiUtente(Model model) {
		// Questa formula si ripete più volte, in pratica estrae un oggetto che conosce
		// i dati usati per il login, ne estrae "l'username"
		// che sarebbe il primo campo del login, e ricerca l'utente tramite quello
		// fino ad ora tentare di recuperare anche il campo "password" è risultato solo
		// in null e nullpointerexception,
		// ma alla fine gli utenti hanno mail uniche quindi basta
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		// ottenuto l'utente si estrae il dato necessario
		model.addAttribute("giochiPosseduti", this.utenteService.trovaListaGiochiUtente(utenteDet.getUsername()));
		return "giochiUtente.html";
	}

	@RequestMapping(value = "/utente/listaConsole", method = RequestMethod.GET)
	public String listaConsoleUtente(Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		model.addAttribute("consolePossedute", this.utenteService.trovaListaConsoleUtente(utenteDet.getUsername()));
		return "consoleUtente.html";
	}

	@RequestMapping(value = "/utente/listaAccessori", method = RequestMethod.GET)
	public String listaAccessoriUtente(Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		model.addAttribute("accessoriPosseduti", this.utenteService.trovaListaAccessoriUtente(utenteDet.getUsername()));
		return "accessoriUtente.html";
	}

	// ------------------------------ AGGIUNTA ATTRIBUTI
	// ------------------------------
	// TODO codice tutto molto simile, essendo una lista unica non si potrebbe
	// generalizzare...?

	@RequestMapping(value = "/utente/iniziaAggiuntaGioco")
	public String iniziaAggiuntaGioco(Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		model.addAttribute("listaGiochiEsistenti", this.giocoService.tutti());
		model.addAttribute("form", new CheckBoxForm());
		return "listaGiochi.html";
	}

	@RequestMapping(value = "/utente/aggiungiGiocoPosseduto", method = RequestMethod.POST)
	public String aggiuntaGiocoUtente(@Valid @ModelAttribute("form") CheckBoxForm gf, Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		Utente utente = this.utenteService.trovaUtente(utenteDet.getUsername());
		for (Long gioco : gf.getIds()) { // trova i giochi corrispondenti agli id passati dalle checkbox
			Gioco giocoPerId = this.giocoService.giocoPerId(gioco);
			Copia c1 = new Copia(giocoPerId);
			this.copiaService.salva(c1);
			utente.addCopia(c1);
		}
		this.utenteService.salva(utente); // ho cambiato "inserisci" in "salva" perché create e update sono la stessa
											// cosa,
											// finché è lo stesso id utente, lo sovrascrive e non ne crea un altro nuovo
											// !! quindi per cambiare dati di qualcosa già nel db è importante l'id
		model.addAttribute("giochiPosseduti", this.utenteService.trovaListaGiochiUtente(utente.getEmail()));
		return "giochiUtente.html";
	}

	@RequestMapping("/vediCondizioni/{id}")
	public String avviaCondizioni(@Valid @PathVariable("id") long id, Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		Copia copia = this.utenteService.getCopia(id, utenteDet.getUsername());
		// "<----OGGETTORIFERIMENTO\n");
		model.addAttribute("condizioni", copia);
		if (copia.getOggettoRiferimento().getClass().equals(Gioco.class)) {
			Gioco g1= (Gioco) copia.getOggettoRiferimento();
			if (g1.getImmagini() != null) {
				for (Immagine i : g1.getImmagini()) {
					switch (i.getTipoImmagine()) {
					case BACKCOVER:
						model.addAttribute("backCover", i);
						break;
					case BANNER:
						model.addAttribute("banner", i);
						break;
					case FANART:
						model.addAttribute("fanArt", i);
						break;
					case FRONTCOVER:
						model.addAttribute("frontCover", i);
						break;
					case LOGO:
						model.addAttribute("logo", i);
						break;
					case SCREENSHOT:
						model.addAttribute("screenshot", i);
						break;
					default:
						break;
					}
				}
			}
			if (g1.getValutazioneEta() != null) {
				switch (g1.getValutazioneEta()) {
				case ADULTSONLY:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/ESRB_Adults_Only_18%2B.svg/200px-ESRB_Adults_Only_18%2B.svg.png");
					break;
				case EVERYONE:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/ESRB_Everyone.svg/200px-ESRB_Everyone.svg.png");
					break;
				case EVERYONE10PLUS:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/ESRB_Everyone_10%2B.svg/200px-ESRB_Everyone_10%2B.svg.png");
					break;
				case MATURE:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/ESRB_Mature_17%2B.svg/200px-ESRB_Mature_17%2B.svg.png");
					break;
				case RATINGPENDING:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/ESRB_RP.svg/200px-ESRB_RP.svg.png");
					break;
				case TEEN:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/ESRB_Teen.svg/200px-ESRB_Teen.svg.png");
					break;
				case EARLYCHILDHOOD:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/ESRB_Early_Childhood.svg/200px-ESRB_Early_Childhood.svg.png");
					break;
				default:
					break;
				}
			}
			model.addAttribute("gioco", copia.getOggettoRiferimento());
			return "gioco.html";
		}
		if (copia.getOggettoRiferimento().getClass().equals(Console.class)) {
			model.addAttribute("console", copia.getOggettoRiferimento());
			return "console.html";
		}
		if (copia.getOggettoRiferimento().getClass().equals(Accessorio.class)) {
			model.addAttribute("accessorio", copia.getOggettoRiferimento());
			return "accessorio.html";
		} else
			return "dashboardUtente.html";
	}

	@RequestMapping("/accettaCondizioni")
	public String accettaCopia(@Valid @ModelAttribute("condizioni") Copia copia,
			@ModelAttribute("copia") Copia copiaOriginale, Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		copiaOriginale.setComprendeExtra(copia.getComprendeExtra());
		copiaOriginale.setComprendeManuale(copia.getComprendeManuale());
		copiaOriginale.setComprendeScatola(copia.getComprendeScatola());
		copiaOriginale.setCondizioniExtra(copia.getCondizioniExtra());
		copiaOriginale.setCondizioniManuale(copia.getCondizioniManuale());
		copiaOriginale.setCondizioniScatola(copia.getCondizioniScatola());
		this.copiaService.salva(copiaOriginale);
		model.addAttribute("condizioni",copiaOriginale);
		if (copiaOriginale.getOggettoRiferimento().getClass().equals(Gioco.class)) {
			Gioco g1= (Gioco) copiaOriginale.getOggettoRiferimento();
			if (g1.getImmagini() != null) {
				for (Immagine i : g1.getImmagini()) {
					switch (i.getTipoImmagine()) {
					case BACKCOVER:
						model.addAttribute("backCover", i);
						break;
					case BANNER:
						model.addAttribute("banner", i);
						break;
					case FANART:
						model.addAttribute("fanArt", i);
						break;
					case FRONTCOVER:
						model.addAttribute("frontCover", i);
						break;
					case LOGO:
						model.addAttribute("logo", i);
						break;
					case SCREENSHOT:
						model.addAttribute("screenshot", i);
						break;
					default:
						break;
					}
				}
			}
			if (g1.getValutazioneEta() != null) {
				switch (g1.getValutazioneEta()) {
				case ADULTSONLY:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/ESRB_Adults_Only_18%2B.svg/200px-ESRB_Adults_Only_18%2B.svg.png");
					break;
				case EVERYONE:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/ESRB_Everyone.svg/200px-ESRB_Everyone.svg.png");
					break;
				case EVERYONE10PLUS:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/ESRB_Everyone_10%2B.svg/200px-ESRB_Everyone_10%2B.svg.png");
					break;
				case MATURE:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/ESRB_Mature_17%2B.svg/200px-ESRB_Mature_17%2B.svg.png");
					break;
				case RATINGPENDING:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/ESRB_RP.svg/200px-ESRB_RP.svg.png");
					break;
				case TEEN:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/ESRB_Teen.svg/200px-ESRB_Teen.svg.png");
					break;
				case EARLYCHILDHOOD:
					model.addAttribute("rating",
							"https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/ESRB_Early_Childhood.svg/200px-ESRB_Early_Childhood.svg.png");
					break;
				default:
					break;
				}
			}
			model.addAttribute("gioco", copiaOriginale.getOggettoRiferimento());
			return "gioco.html";
		}
		if (copiaOriginale.getOggettoRiferimento().getClass().equals(Console.class)) {
			model.addAttribute("console", copiaOriginale.getOggettoRiferimento());
			return "console.html";
		}
		if (copiaOriginale.getOggettoRiferimento().getClass().equals(Accessorio.class)) {
			model.addAttribute("accessorio", copiaOriginale.getOggettoRiferimento());
			return "accessorio.html";
		} else
			return "dashboardUtente.html";
	}

	@RequestMapping("/avviaCondizioni/{id}")
	public String vediCondizioni(@Valid @PathVariable("id") long id, Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		Copia copia = this.utenteService.getCopia(id, utenteDet.getUsername());
		model.addAttribute("condizioni", copia);
		if (copia.getOggettoRiferimento().getClass().equals(Gioco.class)) {
			model.addAttribute("oggetto", (Gioco) copia.getOggettoRiferimento());
		}
		if (copia.getOggettoRiferimento().getClass().equals(Console.class)) {
			model.addAttribute("oggetto",(Console) copia.getOggettoRiferimento());
		}
		if (copia.getOggettoRiferimento().getClass().equals(Accessorio.class)) {
			model.addAttribute("oggetto", (Accessorio) copia.getOggettoRiferimento());
		}
		model.addAttribute("copia", copia);
		return "aggiungiCondizioni.html";
	}

	@RequestMapping(value = "/utente/iniziaAggiuntaConsole")
	public String iniziaAggiuntaConsole(Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		model.addAttribute("listaConsoleEsistenti", this.consoleService.tutti());
		model.addAttribute("form", new CheckBoxForm());
		return "listaConsole.html";
	}

	@RequestMapping(value = "/utente/aggiungiConsolePosseduta", method = RequestMethod.POST)
	public String aggiuntaConsoleUtente(@Valid @ModelAttribute("form") CheckBoxForm gf, Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		Utente utente = this.utenteService.trovaUtente(utenteDet.getUsername());
		for (Long console : gf.getIds()) {
			Console consolePerId = this.consoleService.consolePerId(console);
			Copia c1 = new Copia(consolePerId);
			this.copiaService.salva(c1);
			utente.addCopia(c1);
		}
		this.utenteService.salva(utente);
		model.addAttribute("consolePossedute", this.utenteService.trovaListaConsoleUtente(utente.getEmail()));
		return "consoleUtente.html";
	}

	@RequestMapping(value = "/utente/iniziaAggiuntaAccessorio")
	public String iniziaAggiuntaAccessorio(Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		model.addAttribute("listaAccessoriEsistenti", this.accessorioService.tutti());
		model.addAttribute("form", new CheckBoxForm());
		return "listaAccessori.html";
	}

	@RequestMapping(value = "/utente/aggiungiAccessorioPosseduto", method = RequestMethod.POST)
	public String aggiuntaAccessorioUtente(@Valid @ModelAttribute("form") CheckBoxForm gf, Model model) {
		UserDetails utenteDet = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("role", utenteDet.getAuthorities().iterator().next().getAuthority());
		Utente utente = this.utenteService.trovaUtente(utenteDet.getUsername());
		for (Long accessorio : gf.getIds()) {
			Accessorio accessorioPerId = this.accessorioService.accessorioPerId(accessorio);
			Copia c1 = new Copia(accessorioPerId);
			this.copiaService.salva(c1);
			utente.addCopia(c1);
		}
		this.utenteService.salva(utente);
		model.addAttribute("accessoriPosseduti", this.utenteService.trovaListaAccessoriUtente(utente.getEmail()));
		return "accessoriUtente.html";
	}

	@ModelAttribute("copia")
	public Copia getCopia() {
		return new Copia();
	}

}
