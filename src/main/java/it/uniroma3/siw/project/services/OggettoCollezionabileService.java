package it.uniroma3.siw.project.services;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.project.model.OggettoCollezionabile;
import it.uniroma3.siw.project.repository.OggettoCollezionabileRepository;

@Service
public class OggettoCollezionabileService {
	@Autowired
	private OggettoCollezionabileRepository oggettoCollezionabileRepository;

	public OggettoCollezionabile trovaOggetto(@Valid long id) {
		return this.oggettoCollezionabileRepository.findById(id).get();
	}
}
