package it.uniroma3.siw.project.services;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.project.model.Immagine;

import it.uniroma3.siw.project.repository.ImmagineRepository;

@Service
public class ImmagineService {
	@Autowired
	private ImmagineRepository immagineRepository;

	@Transactional
	public @Valid Immagine inserisci(@Valid Immagine immagine) {
		return this.immagineRepository.save(immagine);
	}
}
