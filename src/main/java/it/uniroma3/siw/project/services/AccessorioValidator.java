package it.uniroma3.siw.project.services;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.siw.project.model.Accessorio;

@Component
public class AccessorioValidator implements Validator {

	@Override
	public boolean supports(Class<?> aClass) {
		return Accessorio.class.equals(aClass);
	}

	@Override
	public void validate(Object arg0, Errors error) {
		ValidationUtils.rejectIfEmptyOrWhitespace(error, "nome", "required");
		Accessorio accessorio = (Accessorio) arg0;
		if (accessorio.getPiattaforma() == null)
			error.reject("piattaforma", "required");

	}

}
