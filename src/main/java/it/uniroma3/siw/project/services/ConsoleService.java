package it.uniroma3.siw.project.services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.project.model.Console;
import it.uniroma3.siw.project.repository.ConsoleRepository;

@Service
public class ConsoleService {
	@Autowired
	private ConsoleRepository consoleRepository;

	public Console consolePerId(Long id) {
		return this.consoleRepository.findById(id).get();
	}

	public Console inserisci(@Valid Console console) {
		return this.consoleRepository.save(console);

	}

	public List<Console> tutti() {
		return (List<Console>) this.consoleRepository.findAll();
	}
}
