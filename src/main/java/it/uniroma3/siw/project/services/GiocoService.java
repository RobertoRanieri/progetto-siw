package it.uniroma3.siw.project.services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.project.model.Gioco;
import it.uniroma3.siw.project.repository.GiocoRepository;

@Service
public class GiocoService {
	@Autowired
	private GiocoRepository giocoRepository;

	public Gioco giocoPerId(Long id) {
		return this.giocoRepository.findById(id).get();
	}

	public Gioco inserisci(@Valid Gioco gioco) {
		return this.giocoRepository.save(gioco);

	}

	public List<Gioco> tutti() {
		return (List<Gioco>) this.giocoRepository.findAll();
	}

}
