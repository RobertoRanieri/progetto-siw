package it.uniroma3.siw.project.services;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.project.model.Copia;
import it.uniroma3.siw.project.model.Utente;
import it.uniroma3.siw.project.repository.UtenteRepository;

@Service
public class UtenteService {
	@Autowired
	private UtenteRepository utenteRepository;
	@Autowired
	private PasswordEncoder passEncoder;

	@Transactional
	public Utente salva(@Valid Utente utente) {
		utente.setPassword(this.passEncoder.encode(utente.getPassword()));
		return this.utenteRepository.save(utente);
	}

	@Transactional
	public Utente trovaUtente(long id) {
		return this.utenteRepository.findById(id).get();
	}

	@Transactional
	public Utente trovaUtente(String email, String psw) {
		Utente utente = this.utenteRepository.findByEmail(email);
		if (this.passEncoder.matches(psw, utente.getPassword()))
			return utente;
		else
			return null;
	}

	@Transactional
	public Utente verificaEsistenzaUtente(String email) {
		return this.utenteRepository.findByEmail(email);
	}

	public Utente trovaUtente(String email) {
		return this.utenteRepository.findByEmail(email);
	}

	public List<Copia> trovaListaGiochiUtente(String email) {
		Utente utente = this.verificaEsistenzaUtente(email);
		if (utente != null)
			return utente.getGiochiPosseduti();
		else
			return new ArrayList<>();
	}

	public List<Copia> trovaListaConsoleUtente(String email) {
		Utente utente = this.verificaEsistenzaUtente(email);
		if (utente != null)
			return utente.getConsolePossedute();
		else
			return new ArrayList<>();
	}

	public List<Copia> trovaListaAccessoriUtente(String email) {
		Utente utente = this.verificaEsistenzaUtente(email);
		if (utente != null)
			return utente.getAccessoriPosseduti();
		else
			return new ArrayList<>();
	}

	public void salvaCopia(String email, @Valid Copia copia) {
		Utente utente = this.verificaEsistenzaUtente(email);
		if (utente != null) {
			utente.addCopia(copia);
			this.salva(utente);
		}
	}

	public Copia getCopia(@Valid long id, String email) {
		Utente utente = this.verificaEsistenzaUtente(email);
		if (utente != null) {
			return utente.getCopia(id);
		}
		return null;
	}

}
