package it.uniroma3.siw.project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import it.uniroma3.siw.project.model.Utente;

@Component
public class UtenteValidator implements Validator {

	@Autowired
	private UtenteService utenteService;

	@Override
	public boolean supports(Class<?> aClass) {
		return Utente.class.equals(aClass);
	}

	@Override
	public void validate(Object arg0, Errors error) {
		ValidationUtils.rejectIfEmptyOrWhitespace(error, "email", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(error, "password", "required");
		Utente utente = (Utente) arg0;
		if (!utente.getEmail().matches(".*@.*[.].*"))
			error.rejectValue("email", "badmail");
	}

	public void registrabile(Object arg0, Errors error) {
		Utente utente = (Utente) arg0;
		if (utenteService.verificaEsistenzaUtente(utente.getEmail()) != null)
			error.rejectValue("email", "duplicateEmail");
	}
}