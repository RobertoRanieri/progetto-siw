package it.uniroma3.siw.project.services;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.project.model.Copia;
import it.uniroma3.siw.project.repository.CopiaRepository;

@Service
public class CopiaService {
	@Autowired
	private CopiaRepository copiaRepository;

	public Copia trovaCopia(@Valid long id) {
		return this.copiaRepository.findById(id).get();
	}

	public Copia salva(Copia copia) {
		return this.copiaRepository.save(copia);
	}

}
