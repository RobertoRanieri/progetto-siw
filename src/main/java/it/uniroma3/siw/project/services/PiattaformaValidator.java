package it.uniroma3.siw.project.services;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import it.uniroma3.siw.project.model.Piattaforma;

@Component
public class PiattaformaValidator implements Validator {

	@Override
	public boolean supports(Class<?> aClass) {
		return Piattaforma.class.equals(aClass);
	}

	@Override
	public void validate(Object arg0, Errors error) {
		ValidationUtils.rejectIfEmptyOrWhitespace(error, "nome", "required");
	}

}