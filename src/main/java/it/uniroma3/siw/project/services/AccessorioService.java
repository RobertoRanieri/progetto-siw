package it.uniroma3.siw.project.services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.project.model.Accessorio;
import it.uniroma3.siw.project.repository.AccessorioRepository;

@Service
public class AccessorioService {
	@Autowired
	private AccessorioRepository accessorioRepository;

	public Accessorio accessorioPerId(Long id) {
		return this.accessorioRepository.findById(id).get();
	}

	public Accessorio inserisci(@Valid Accessorio accessorio) {
		return this.accessorioRepository.save(accessorio);

	}

	public List<Accessorio> tutti() {
		return (List<Accessorio>) this.accessorioRepository.findAll();
	}

}
