package it.uniroma3.siw.project.services;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import it.uniroma3.siw.project.model.Piattaforma;
import it.uniroma3.siw.project.repository.PiattaformaRepository;

@Service
public class PiattaformaService {
	@Autowired
	private PiattaformaRepository piattaformaRepository;

	@Transactional
	public @Valid Piattaforma inserisci(@Valid Piattaforma piattaforma) {
		return this.piattaformaRepository.save(piattaforma);
	}

	@Transactional
	public Piattaforma trovaPiattaforma(long id) {
		return this.piattaformaRepository.findById(id).get();
	}

	@Transactional
	public Piattaforma trovaPiattaforma(String nome) {
		return this.piattaformaRepository.findByNome(nome);
	}

	public List<Piattaforma> tutti() {
		return (List<Piattaforma>) this.piattaformaRepository.findAll();
	}

}
