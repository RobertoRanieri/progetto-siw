package it.uniroma3.siw.project.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Immagine {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String url;
	private TipoImmagine tipoImmagine;

	// costruttori

	public Immagine() {
	}

	public Immagine(String url, TipoImmagine tipoImmagine) {
		this.url = url;
		this.tipoImmagine = tipoImmagine;
	}

	// getters&setters

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public TipoImmagine getTipoImmagine() {
		return tipoImmagine;
	}

	public void setTipoImmagine(TipoImmagine tipoImmagine) {
		this.tipoImmagine = tipoImmagine;
	}

}
