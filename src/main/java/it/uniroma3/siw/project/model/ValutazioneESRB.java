package it.uniroma3.siw.project.model;

public enum ValutazioneESRB {
	EARLYCHILDHOOD, EVERYONE, EVERYONE10PLUS, TEEN, MATURE, ADULTSONLY, RATINGPENDING;
}
