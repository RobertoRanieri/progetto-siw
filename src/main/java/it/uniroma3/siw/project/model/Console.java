package it.uniroma3.siw.project.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Console extends OggettoCollezionabile {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private long id;
	private String nome;

	@ManyToOne
	private Piattaforma piattaforma;

	@OneToOne
	private Immagine immagine;

	// costruttori

	public Console() {
	}

	public Console(String nome, Piattaforma piattaforma, Immagine immagine) {
		super();
		this.nome = nome;
		this.piattaforma = piattaforma;
		this.immagine = immagine;
	}

	// getters&setters

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String getNome() {
		return nome;
	}

	@Override
	public void setNome(String nome) {
		this.nome = nome;
	}

	public Piattaforma getPiattaforma() {
		return piattaforma;
	}

	public void setPiattaforma(Piattaforma piattaforma) {
		this.piattaforma = piattaforma;
	}

	public Immagine getImmagine() {
		return immagine;
	}

	public void setImmagine(Immagine immagine) {
		this.immagine = immagine;
	}

}
