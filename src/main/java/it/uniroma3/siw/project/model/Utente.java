package it.uniroma3.siw.project.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.Valid;

@Entity
public class Utente {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String email;
	private String password;
	private LocalDateTime dataIscrizione;
	private String role;

	/*
	 * @OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL) private
	 * Map<OggettoCollezionabile, Condizioni> oggettiInCollezione;
	 */
	@OneToMany(fetch = FetchType.EAGER)
	private List<Copia> copie;

	// costuttori

	public Utente() {
		this.dataIscrizione = LocalDateTime.now();
		// this.oggettiInCollezione = new HashMap<>();
		this.copie = new ArrayList<>();
	}

	public Utente(String email, String password, String role) {
		this();
		this.email = email;
		this.password = password;
		this.role = role;
	}

	// getters&setters

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDateTime getDataIscrizione() {
		return dataIscrizione;
	}

	public void setDataIscrizione(LocalDateTime dataIscrizione) {
		this.dataIscrizione = dataIscrizione;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	/*
	 * public Map<OggettoCollezionabile, Condizioni> getOggettiInCollezione() {
	 * return oggettiInCollezione; }
	 * 
	 * public void aggiungiOggettoInCollezione(OggettoCollezionabile oggetto,
	 * Condizioni condizioni) { this.oggettiInCollezione.put(oggetto, condizioni); }
	 * 
	 * public void setOggettiInCollezione(Map<OggettoCollezionabile, Condizioni>
	 * oggettiInCollezione) { this.oggettiInCollezione = oggettiInCollezione; }
	 */

	public Copia getCopia(@Valid long id) {
		for (Copia copia : this.copie) {
			if (copia.getId() == id) {
				return copia;
			}
		}
		return null;
	}

	public List<Copia> getCopie() {
		return this.copie;
	}

	public void setCopia(List<Copia> copia) {
		this.copie = copia;
	}

	public void addCopia(Copia copia) {
		this.copie.add(copia);
	}

	public List<Copia> getGiochiPosseduti() {
		List<Copia> lista = new ArrayList<>();
		for (Copia copia : this.copie) {
			OggettoCollezionabile oggettoRiferimento = copia.getOggettoRiferimento();
			if (oggettoRiferimento.getClass().equals(Gioco.class))
				lista.add(copia);
		}
		return lista;
	}

	public List<Copia> getConsolePossedute() {
		List<Copia> lista = new ArrayList<>();
		for (Copia copia : this.copie) {
			OggettoCollezionabile oggettoRiferimento = copia.getOggettoRiferimento();
			if (oggettoRiferimento.getClass().equals(Console.class))
				lista.add(copia);
		}
		return lista;
	}

	public List<Copia> getAccessoriPosseduti() {
		List<Copia> lista = new ArrayList<>();
		for (Copia copia : this.copie) {
			OggettoCollezionabile oggettoRiferimento = copia.getOggettoRiferimento();
			if (oggettoRiferimento.getClass().equals(Accessorio.class))
				lista.add(copia);
		}
		return lista;
	}

}
