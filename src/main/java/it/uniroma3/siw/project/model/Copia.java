package it.uniroma3.siw.project.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Copia {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne(fetch = FetchType.EAGER)
	private OggettoCollezionabile oggettoRiferimento;

	private int condizioniOggetto;

	private boolean comprendeManuale;
	private int condizioniManuale;

	private boolean comprendeScatola;
	private int condizioniScatola;

	private boolean comprendeExtra;
	private int condizioniExtra;

	// Valuta le condizioni generali del gioco
	/*
	 * TODO private int valuta() { if(this.comprendeManuale && this.comprendeScatola
	 * && this.comprendeExtra) {
	 * if((this.condizioniOggetto*0.5)+(this.condizioniManuale*0.25)+(this.
	 * condizioniScatola*0.25)+(this.condizioniExtra*0.1)>=10) return 10; else {
	 * return (int)
	 * ((this.condizioniOggetto*0.5)+(this.condizioniManuale*0.25)+(this.
	 * condizioniScatola*0.25)+(this.condizioniExtra*0.1)); }
	 * }if(this.comprendeManuale ) }
	 */

	public Copia() {
		this.comprendeExtra = false;
		this.comprendeManuale = false;
		this.comprendeScatola = false;
	}

	public Copia(OggettoCollezionabile o) {
		this();
		this.oggettoRiferimento = o;
	}

	public Copia(int condizioniOggetto, boolean comprendeManuale, int condizioniManuale, boolean comprendeScatola,
			int condizioniScatola, boolean comprendeExtra, int condizioniExtra) {
		super();
		this.condizioniOggetto = condizioniOggetto;
		this.comprendeManuale = comprendeManuale;
		this.condizioniManuale = condizioniManuale;
		this.comprendeScatola = comprendeScatola;
		this.condizioniScatola = condizioniScatola;
		this.comprendeExtra = comprendeExtra;
		this.condizioniExtra = condizioniExtra;
	}

	public int getCondizioniOggetto() {
		return condizioniOggetto;
	}

	public long getId() {
		return id;
	}

	public void setCondizioniOggetto(int condizioniOggetto) {
		this.condizioniOggetto = condizioniOggetto;
	}

	public boolean isComprendeManuale() {
		return comprendeManuale;
	}

	public boolean getComprendeManuale() {
		return comprendeManuale;
	}

	public void setComprendeManuale(boolean comprendeManuale) {
		this.comprendeManuale = comprendeManuale;
	}

	public int getCondizioniManuale() {
		return condizioniManuale;
	}

	public void setCondizioniManuale(int condizioniManuale) {
		this.condizioniManuale = condizioniManuale;
	}

	public boolean isComprendeScatola() {
		return comprendeScatola;
	}

	public boolean getComprendeScatola() {
		return comprendeScatola;
	}

	public void setComprendeScatola(boolean comprendeScatola) {
		this.comprendeScatola = comprendeScatola;
	}

	public int getCondizioniScatola() {
		return condizioniScatola;
	}

	public void setCondizioniScatola(int condizioniScatola) {
		this.condizioniScatola = condizioniScatola;
	}

	public boolean isComprendeExtra() {
		return comprendeExtra;
	}

	public boolean getComprendeExtra() {
		return comprendeExtra;
	}

	public void setComprendeExtra(boolean comprendeExtra) {
		this.comprendeExtra = comprendeExtra;
	}

	public int getCondizioniExtra() {
		return condizioniExtra;
	}

	public void setCondizioniExtra(int condizioniExtra) {
		this.condizioniExtra = condizioniExtra;
	}

	public OggettoCollezionabile getOggettoRiferimento() {
		return this.oggettoRiferimento;
	}

	public void setOggettoRiferimento(OggettoCollezionabile oggettoRiferimento) {
		this.oggettoRiferimento = oggettoRiferimento;
	}
}
