package it.uniroma3.siw.project.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

//TODO fetch e cascade sulle relazioni che lo necessitano
@Entity
public class Gioco extends OggettoCollezionabile {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private long id;

	@Column(nullable = false)
	private String nome;

	private LocalDate dataRilascio;

	@ManyToOne
	private Piattaforma piattaforma;

	@Column(columnDefinition = "VARCHAR(2048)")
	private String descrizione;

	private ValutazioneESRB valutazioneEta;

	private String genere;

	private int numeroMassimoGiocatori;

	private boolean haModalitaCoop;

	private String linkVideoYoutube;

	private String editore;

	private String sviluppatore;

	@OneToMany(fetch = FetchType.EAGER)
	private List<Immagine> immagini;

	// costruttori

	public Gioco() {
		this.immagini = new ArrayList<Immagine>();
	}

	public Gioco(String nome) {
		this();
		this.nome = nome;
	}

	public Gioco(String nome, LocalDate dataRilascio, Piattaforma piattaforma, String descrizione,
			ValutazioneESRB valutazioneEta, String genere, int numeroMassimoGiocatori, boolean haModalitaCoop,
			String linkVideoYoutube, String editore, String sviluppatore, List<Immagine> immagini) {
		this.nome = nome;
		this.dataRilascio = dataRilascio;
		this.piattaforma = piattaforma;
		this.descrizione = descrizione;
		this.valutazioneEta = valutazioneEta;
		this.genere = genere;
		this.numeroMassimoGiocatori = numeroMassimoGiocatori;
		this.haModalitaCoop = haModalitaCoop;
		this.linkVideoYoutube = linkVideoYoutube;
		this.editore = editore;
		this.sviluppatore = sviluppatore;
		this.immagini = immagini;
	}

	// getters&setters

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String getNome() {
		return nome;
	}

	@Override
	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDate getDataRilascio() {
		return dataRilascio;
	}

	public void setDataRilascio(LocalDate dataRilascio) {
		this.dataRilascio = dataRilascio;
	}

	public Piattaforma getPiattaforma() {
		return piattaforma;
	}

	public void setPiattaforma(Piattaforma piattaforma) {
		this.piattaforma = piattaforma;
	}

	@Column(columnDefinition = "VARCHAR(2048)")
	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public ValutazioneESRB getValutazioneEta() {
		return valutazioneEta;
	}

	public void setValutazioneEta(ValutazioneESRB valutazioneEta) {
		this.valutazioneEta = valutazioneEta;
	}

	public String getGenere() {
		return genere;
	}

	public void setGenere(String genere) {
		this.genere = genere;
	}

	public int getNumeroMassimoGiocatori() {
		return numeroMassimoGiocatori;
	}

	public void setNumeroMassimoGiocatori(int numeroMassimoGiocatori) {
		this.numeroMassimoGiocatori = numeroMassimoGiocatori;
	}

	public boolean isHaModalitaCoop() {
		return haModalitaCoop;
	}

	public void setHaModalitaCoop(boolean haModalitaCoop) {
		this.haModalitaCoop = haModalitaCoop;
	}

	public String getLinkVideoYoutube() {
		return linkVideoYoutube;
	}

	public void setLinkVideoYoutube(String linkVideoYoutube) {
		this.linkVideoYoutube = linkVideoYoutube;
	}

	public String getEditore() {
		return editore;
	}

	public void setEditore(String editore) {
		this.editore = editore;
	}

	public String getSviluppatore() {
		return sviluppatore;
	}

	public void setSviluppatore(String sviluppatore) {
		this.sviluppatore = sviluppatore;
	}

	public List<Immagine> getImmagini() {
		return immagini;
	}

	public void addImmagini(Immagine immagine) {
		this.immagini.add(immagine);
	}
}