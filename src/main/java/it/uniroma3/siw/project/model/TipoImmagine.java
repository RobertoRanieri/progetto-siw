package it.uniroma3.siw.project.model;

public enum TipoImmagine {
	FRONTCOVER, BACKCOVER, SCREENSHOT, LOGO, FANART, BANNER, PHOTO;
}
